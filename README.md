# Gestion de projet/Git TP noté

## Objectifs

Appliquer la procédure ci-dessous :

- Dans ce dépôt, créer un **ticket** (Issue) à vos nom et prénom (conserver le numéro de ticket)
- **Forker** ce dépôt depuis votre compte GitLab (votre fork doit être en _Visibility **Public**_)

- Depuis votre fork, créer une **branche** de travail **_feature/#X_** (remplacer X par votre numéro de ticket)

- **Récupérer un zip** des sources (bouton _Code_, _Download source code_) et le **décompresser** sur votre ordinateur
- Ouvrir **_qcm.html_** dans un **navigateur**
- **Répondre** aux questions et valider
- Copier les réponses dans un fichier txt nommé avec **votre nom et prénom** et le mettre dans le dossier **'_rep_'**.
- **Commiter** vos modifications en les rattachant à votre ticket

- Créer le répertoire **TP\_#X** (remplacer X par votre numéro de ticket) et y placer un fichier _index.html_ et tous
  les contenus nécessaires (à votre convenance)
- **Commiter** vos modifications en les rattachant à votre ticket

- **Naviguer** sur https://creativecommons.org/choose/ et **choisir** une licence qui vous convient
- Ajouter le **code html de la licence** choisie dans votre _index.html_
- Ajouter un fichier de **Licence** (qui couvre le dépôt)
- Ajouter un fichier de **ChangeLog**
- **Commiter** vos modifications en les rattachant à votre ticket

- Créer la **demande de fusion** en la rattachant à votre ticket et en demandant la cloture du ticket

## Barème d'évaluation

- Questions du QCM partie 1 => note Gestion de projet séparée sur 20
- Questions du QCM partie 2 : 10 points
- Présence du ticket (ticket clos, commits présents, merge request présente) : 6 points
- Validité du fichier index.html : 1 points
- Présence de la licence : 1 point
- Présence du ChangeLog : 1 point
- Appréciation (subjective) de votre page web : 1 point
- => note Git sur 20

_**Avant de partir, assurez-vous auprès de l'intervenant que votre demande de fusion a bien été prise en compte.**_
